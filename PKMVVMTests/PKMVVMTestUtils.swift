//
//  PKMVVMTestUtils.swift
//  PKMVVMTests
//
//  Created by Divya Kothagattu on 05/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//
import UIKit
import XCTest

@testable import PKMVVM
class PKMVVMTestUtils {

    static func loadData(_ fileName: String) -> Data? {
        guard let filePath = Bundle.main.path(forResource: fileName, ofType: "json"), let data = NSData(contentsOfFile: filePath) ,
            let dataObj = data as Data? else {
            return nil
            }
           return dataObj
    }

    static func loadJSON(_ fileName: String) -> [AnyHashable: Any] {
        let errorMsg = "Bad or failed JSON parsing for \(fileName).json"
        guard let data = PKMVVMTestUtils.loadData(fileName) else {
            return [:]
        }
        do {
            guard let data = try JSONSerialization.jsonObject(with: data, options: []) as? [AnyHashable: Any] else {
                return [:]
            }
            return data
        } catch {
            XCTFail(errorMsg)
        }
        return [:]
    }
}
