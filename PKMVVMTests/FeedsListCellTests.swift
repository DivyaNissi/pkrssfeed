//
//  FeedsListCellTests.swift
//  PKMVVMTests
//
//  Created by Divya Kothagattu on 05/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import XCTest
import UIKit

@testable import PKMVVM

class FeedsListCellTests: XCTestCase {
    var items = [Entry]()
    var viewControllerUnderTest: FeedsViewController?
    var tableView: UITableView!
    private var dataSource: TableViewDataSource!
    private weak var delegate: TableViewDelegate!
    var feedListCell: FeedsListCell?
    override func setUp() {
        super.setUp()
        guard let viewController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:"FeedsViewController") as? FeedsViewController else {
            return XCTFail("Could not instantiate FeedsViewController from FeedsViewController storyboard")
        }
        self.viewControllerUnderTest = viewController
        self.viewControllerUnderTest?.loadView()
        self.viewControllerUnderTest?.viewDidLoad()
        self.viewControllerUnderTest?.feedsView.awakeFromNib()
        self.viewControllerUnderTest?.feedsViewModel.getUpdatedMovies()
        self.viewControllerUnderTest?.feedsView.feedsTableView.reloadData()

        tableView = UITableView(frame: CGRect(x: 0, y: 0, width: 200, height: 400), style: .plain)
        let itemXib = UINib.init(nibName: "FeedsListCell",
                                 bundle: nil)
        tableView.register(itemXib,
                           forCellReuseIdentifier: "FeedsListCell")
        dataSource = TableViewDataSource()
        tableView.delegate = delegate
        tableView.dataSource = dataSource
    }

    func testAwakeFromNib() {
        super.awakeFromNib()
        testPopulateMoviesInfo()
    }
    func loadJSON(_ fileName: String) -> [Entry] {
        let errorMsg = "Bad or failed JSON parsing for \(fileName).json"
        guard let data = PKMVVMTestUtils.loadData(fileName) else {
            return []
        }
        do {
            let feeds = try JSONDecoder().decode(FeedsModel.self, from: data)
            return feeds.feed.entry
        } catch {
            XCTFail(errorMsg)
        }
        XCTAssertNoThrow(try JSONDecoder().decode(FeedsModel.self, from: data))
        return []
    }
    func testPopulateMoviesInfo() {
        var temPEntry: [Entry]?
        temPEntry  = loadJSON("FeedModel")
        if let _ = temPEntry, let count = temPEntry?.count, count > 0 {
            items = temPEntry!
            dataSource.items = items
           // viewControllerUnderTest?.feedsView.didUpdateMovies(movies: items)
            tableView.reloadData()
        }
    }
    func testTextDidChange() {
        viewControllerUnderTest?.feedsView.searchBar((viewControllerUnderTest?.feedsView.searchBar)!, textDidChange: "K")
    }
    func testSearchBarCancelButtonClicked() {
        viewControllerUnderTest?.feedsView.searchBarCancelButtonClicked((viewControllerUnderTest?.feedsView.searchBar)!)
        XCTAssert(true, "Cancel button tapped")
       }
    func testTestSearchBarTextDidEndEditing() {
        viewControllerUnderTest?.feedsView.searchBarTextDidEndEditing((viewControllerUnderTest?.feedsView.searchBar)!)
        XCTAssert(true, "Text didend editing tapped")
    }
    func testSearchBarSearchButtonClicked() {
        viewControllerUnderTest?.feedsView.searchBarSearchButtonClicked((viewControllerUnderTest?.feedsView.searchBar)!)
        XCTAssert(true, "searchBar search button tapped")
    }
}

private class TableViewDataSource: NSObject, UITableViewDataSource {
    var items = [Entry]()
    var feedListCell: FeedsListCell?
    override init() {
         super.init()
     }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return UITableView.automaticDimension
       }
}
private class TableViewDelegate: NSObject, UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

