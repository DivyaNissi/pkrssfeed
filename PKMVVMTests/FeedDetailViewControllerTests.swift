//
//  FeedDetailViewControllerTests.swift
//  PKMVVMTests
//
//  Created by Divya Kothagattu on 06/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import XCTest
import UIKit

@testable import PKMVVM

class FeedDetailViewControllerTests: XCTestCase {
     var items = [Entry]()
    var feedDetailsVc : FeedDetailViewController = FeedDetailViewController()

    override func setUp() {
        super.setUp()
        feedDetailsVc.viewDidLoad()
        XCTAssertNotNil(feedDetailsVc.view)
        XCTAssertNotNil(feedDetailsVc.viewDidLoad())
        XCTAssertNotNil(feedDetailsVc.backButtonTapped())
    }
    func loadJSON(_ fileName: String) -> [Entry] {
        let errorMsg = "Bad or failed JSON parsing for \(fileName).json"
        guard let data = PKMVVMTestUtils.loadData(fileName) else {
            return []
        }
        do {
            let feeds = try JSONDecoder().decode(FeedsModel.self, from: data)
            return feeds.feed.entry
        } catch {
            XCTFail(errorMsg)
        }
        XCTAssertNoThrow(try JSONDecoder().decode(FeedsModel.self, from: data))
        return []
    }
    func testPopulateMoviesInfo() {
        var temPEntry: [Entry]?
        temPEntry  = loadJSON("FeedModel")
        if let _ = temPEntry, let count = temPEntry?.count, count > 0 {
            items = temPEntry!
            feedDetailsVc.populateDetail(items[0])
        }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
