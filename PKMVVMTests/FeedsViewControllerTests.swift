//
//  FeedsViewControllerTests.swift
//  PKMVVMTests
//
//  Created by Divya Kothagattu on 05/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import XCTest
import Foundation
import UIKit

@testable import PKMVVM

class FeedsViewControllerTests: XCTestCase {
    var items = [Entry]()
   var feedVc :FeedsViewController = FeedsViewController()
    override func setUp() {
        super.setUp()
        feedVc.viewDidLoad()
        XCTAssertNotNil(feedVc.view)
        XCTAssertNotNil(feedVc.feedsViewModel)
        XCTAssertNotNil(feedVc.viewDidLoad())
    }
    func testFeedsViewModel() {
        guard PKMVVMTestUtils.loadJSON("FeedModel") is [String: Any] else {
            XCTFail("Invalid Response")
            return
        }
    }
    func loadJSON(_ fileName: String) -> [Entry] {
        let errorMsg = "Bad or failed JSON parsing for \(fileName).json"
        guard let data = PKMVVMTestUtils.loadData(fileName) else {
            return []
        }
        do {
            let feeds = try JSONDecoder().decode(FeedsModel.self, from: data)
            return feeds.feed.entry
        } catch {
            XCTFail(errorMsg)
        }
        XCTAssertNoThrow(try JSONDecoder().decode(FeedsModel.self, from: data))
        return []
    }

    func testPopulateMoviesInfo() {
        var temPEntry: [Entry]?
        temPEntry  = loadJSON("FeedModel")
        if let _ = temPEntry, let count = temPEntry?.count, count > 0 {
            items = temPEntry!
            feedVc.didSelectRow(temPEntry![0])
        }
    }
    func testDidTappedOnSearch() {
        feedVc.didTappedOnSsearch(searchActive: false)
        XCTAssertNotNil("Could not load didTapped on Ssearch method at first time")
    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
