//
//  FeedDetailViewCellTests.swift
//  PKMVVMTests
//
//  Created by Divya Kothagattu on 06/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import XCTest
import UIKit

@testable import PKMVVM

class FeedDetailViewCellTests: XCTestCase {

    var items = [Entry]()
    var feedDetailsVc: FeedDetailViewController?
    var feedDetailTableView: UITableView!
    private var dataSource: TableViewDataSource!
//    private weak var delegate: TableViewDelegate!

    var feedDetailsCell: FeedDetailViewCell?
    override func setUp() {
        super.setUp()
               guard let viewController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:"FeedDetailViewController") as? FeedDetailViewController else {
                   return XCTFail("Could not instantiate FeedsViewController from FeedsViewController storyboard")
               }
               self.feedDetailsVc = viewController
               self.feedDetailsVc?.loadView()
               self.feedDetailsVc?.viewDidLoad()
               self.feedDetailsVc?.detailsView.awakeFromNib()
               self.feedDetailsVc?.detailsView.feedsDetailsTableView.reloadData()
               feedDetailTableView = UITableView(frame: CGRect(x: 0, y: 0, width: 200, height: 400), style: .plain)
               let itemXib = UINib.init(nibName: "FeedDetailViewCell",
                                        bundle: nil)
               feedDetailTableView.register(itemXib,
                                  forCellReuseIdentifier: "FeedDetailViewCell")
        dataSource = TableViewDataSource()
        feedDetailTableView.dataSource = dataSource
    }
    func testAwakeFromNib() {
        feedDetailsCell?.awakeFromNib()
        testPopulateMoviesDetailsInfo()
    }
    func testPoplatestImage() {
        feedDetailsCell?.setUpImage(items[0])
        feedDetailsCell?.populateMoviesDetailsInfo(items[0])
    }
    func loadJSON(_ fileName: String) -> [Entry] {
        let errorMsg = "Bad or failed JSON parsing for \(fileName).json"
        guard let data = PKMVVMTestUtils.loadData(fileName) else {
            return []
        }
        do {
            let feeds = try JSONDecoder().decode(FeedsModel.self, from: data)
            return feeds.feed.entry
        } catch {
            XCTFail(errorMsg)
        }
        XCTAssertNoThrow(try JSONDecoder().decode(FeedsModel.self, from: data))
        return []
    }

    func testPopulateMoviesDetailsInfo() {
        var temPEntry: [Entry]?
        temPEntry  = loadJSON("FeedModel")
        if let _ = temPEntry, let count = temPEntry?.count, count > 0 {
            items = temPEntry!
            feedDetailTableView.reloadData()
        } else {
            return
        }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
private class TableViewDataSource: NSObject, UITableViewDataSource {
    var items = [Entry]()
    var feedDetailsCell: FeedDetailViewCell?
    override init() {
        super.init()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
