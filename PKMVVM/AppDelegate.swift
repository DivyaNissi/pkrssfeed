//
//  AppDelegate.swift
//  PKMVVM
//
//  Created by Divya Kothagattu on 02/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit
import ADEUMInstrumentation
import SplunkMint

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let config = ADEumAgentConfiguration(appKey: "AD-AAB-AAV-FSZ")
        config.collectorURL = "https://col.eum-appdynamics.com"
        // config.screenshotUrl = "https://image.eum-appdynamics.com"
        ADEumInstrumentation.initWith(config)
         Mint.sharedInstance()?.initAndStartSession(withAPIKey: "45463dee")
        Mint.sharedInstance()?.enableLogging(true)
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

}
