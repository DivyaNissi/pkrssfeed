//
// PKAlertView.swift
//  PKMVVM
//
//  Created by Divya Kothagattu on 04/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import Foundation
import UIKit

func topMostController() -> UIViewController? {
    guard let window = UIApplication.shared.windows.last, let rootViewController = window.rootViewController else {
        return nil
    }

    var topController = rootViewController

    while let newTopController = topController.presentedViewController {
        topController = newTopController
    }

    return topController
}

protocol PKAlertViewControllerProtocol: class {
    static func submitAlertView(viewController: UIViewController, title: String, message: String, completionHandler: @escaping(Bool) -> Void)
}

extension UIAlertController: PKAlertViewControllerProtocol {

    static func submitAlertView(viewController: UIViewController, title: String, message: String, completionHandler: @escaping(Bool) -> Void) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        let submitAction = UIAlertAction(title: "OK", style: .default, handler: { (_ ) -> Void in
            alert.dismiss(animated: true, completion: nil)
        })
        alert.addAction(submitAction)
        topMostController()?.present(alert, animated: true) {
            completionHandler(true)
        }
    }
}
