//
// HudProgressViewProtocol.swift
//  PKMVVM
//
//  Created by Divya Kothagattu on 04/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import Foundation
import  UIKit

protocol HudProgressViewProtocol {
    var containerView: UIView { get set }
    var activityIndicator: UIActivityIndicatorView { get set }
    var loadingView: UIView { get set }
    func showHudProgressView(onView: UIView?)
    func hideHudProgressView()
}
