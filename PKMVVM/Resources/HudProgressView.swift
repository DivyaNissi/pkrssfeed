//
// HudProgressView.swift
//  PKMVVM
//
//  Created by Divya Kothagattu on 04/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit

class HudProgressView: HudProgressViewProtocol {

    var containerView = UIView()
    var activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
    var loadingView: UIView = UIView()
    static let shared = HudProgressView()

    func showHudProgressView(onView: UIView? = nil) {
        if let window = UIApplication.shared.windows.first {
            var container = onView
            if  onView == nil {
                container = window
            }
            guard let view = container else {
                return
            }
            containerView.frame = window.frame
            containerView.center = window.center
            containerView.backgroundColor = UIColor.clear
            containerView.tag = 789465
            loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
            loadingView.center = view.center
            loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            loadingView.clipsToBounds = true
            loadingView.layer.masksToBounds = true
            loadingView.layer.cornerRadius = 10
            activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
            activityIndicator.style = UIActivityIndicatorView.Style.large
            activityIndicator.color = #colorLiteral(red: 0.8414117518, green: 0.06093361637, blue: 0.5584653756, alpha: 1)
            activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2,
                                               y: loadingView.frame.size.height / 2)
            loadingView.addSubview(activityIndicator)
            containerView.addSubview(loadingView)
            view.addSubview(containerView)
            activityIndicator.startAnimating()
        }
    }

    func hideHudProgressView() {
        DispatchQueue.main.async {
            self.loadingView.removeFromSuperview()
            self.activityIndicator.stopAnimating()
            self.containerView.removeFromSuperview()
        }
    }
}
