//
//  FeedsView.swift
//  PKMVVM
//
//  Created by Divya Kothagattu on 02/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit

protocol SelectedFeedDelegate: class {
    func  didSelectRow(_ entry: Entry?)
}

protocol SearchTappedDelegate: class {
    func  didTappedOnSsearch(searchActive: Bool)
}

class FeedsView: UIView  { // UpdatedDataProtocol
    @IBOutlet weak var feedsTableView: UITableView!

    @IBOutlet var searchBar: UISearchBar! {
        didSet {
            searchBar.returnKeyType = .done
            searchBar.enablesReturnKeyAutomatically = false
            searchBar.delegate = self
            searchBar.showsCancelButton = true
        }
    }

    private(set) var searchArray = [Entry]()
    private(set) var searchActive : Bool = false
    private(set) var moviesArray = [Entry]()
    var featchHandler: FetchDataProtocol?
     weak var selectedFeedDelegate: SelectedFeedDelegate?
     weak var searchTappedDelegate: SearchTappedDelegate?

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
//        featchHandler?.getUpdatedMovies()
    }

 //   func didUpdateMovies(movies: [Entry]) {
//        moviesArray = movies
//        DispatchQueue.main.async {
//            self.feedsTableView.reloadData()
//        }
 //}
}

extension FeedsView : UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        searchActive == true ? searchArray.count : moviesArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FeedsListCell.self), for: indexPath) as? FeedsListCell {
            cell.selectionStyle = .none
            if searchActive {
                cell.populateMoviesInfo(searchArray[indexPath.row])
            } else {
                cell.populateMoviesInfo(moviesArray[indexPath.row])
            }
            return cell
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension FeedsView: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchActive {
            self.selectedFeedDelegate?.didSelectRow(searchArray[indexPath.row])
        } else {
            self.selectedFeedDelegate?.didSelectRow(moviesArray[indexPath.row])
        }
    }

}

extension FeedsView: UISearchDisplayDelegate {

}
extension FeedsView: UISearchBarDelegate {

     func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }

     func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }

     func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.text = nil
        self.feedsTableView.reloadData()
        searchBar.endEditing(true)
    }

     func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.endEditing(true)
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    let filteredArray = self.moviesArray.filter { ($0.title?.label.contains(searchText))!}
    searchArray = filteredArray
    searchActive = searchText.isEmpty ? false : true
    self.feedsTableView.reloadData()
    }
}

extension FeedsView: DataSource {
    func handleMovies(movies: [Entry]) {
        moviesArray = movies
        DispatchQueue.main.async {
            self.feedsTableView.reloadData()
        }
    }
}
