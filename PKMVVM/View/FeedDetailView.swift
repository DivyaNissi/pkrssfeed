//
//  FeedDetailView.swift
//  PKMVVM
//
//  Created by Divya Kothagattu on 04/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit

class FeedDetailView: UIView {
     var dataSource: Entry?
    @IBOutlet weak var feedsDetailsTableView: UITableView!

    override func awakeFromNib() {
        super.awakeFromNib()
        feedsDetailsTableView.estimatedRowHeight = 44.0
    }

    func popuLateData(_ entry: Entry?) {
        if let entry = entry {
            dataSource = entry
            DispatchQueue.main.async {
                      self.feedsDetailsTableView.reloadData()
                  }
        }
    }
}
extension FeedDetailView : UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FeedDetailViewCell.self), for: indexPath) as? FeedDetailViewCell {
            cell.selectionStyle = .none

            if let data = dataSource {
                cell.populateMoviesDetailsInfo(data)
            }
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}
