//
//  FeedsListCell.swift
//  PKMVVM
//
//  Created by Divya Kothagattu on 04/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit
import Kingfisher
import SplunkMint

class FeedsListCell: UITableViewCell {
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var releaseDateLable: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func populateMoviesInfo(_ entry: Entry) {
        titleLabel?.text = entry.title?.label
        let color: UIColor = #colorLiteral(red: 0.8414117518, green: 0.06093361637, blue: 0.5584653756, alpha: 1)
        titleLabel?.textColor = color
        desLabel?.text = entry.category?.attributes.label
        if let imageString = entry.imImage?.last?.label {
            if let url = URL(string: imageString) {
//                leftImageView.kf.indicatorType = .activity
                leftImageView.kf.setImage(with: url)
            }
        }
        releaseDateLable.text = entry.imReleaseDate?.attributes.label
        Mint.sharedInstance()?.logEvent(withName: "feedsCell")
    }
}
