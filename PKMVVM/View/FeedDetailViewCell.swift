//
//  FeedDetailViewCell.swift
//  PKMVVM
//
//  Created by Divya Kothagattu on 04/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit
import SplunkMint
import Kingfisher

class FeedDetailViewCell: UITableViewCell {

    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var releaseDateLable: UILabel!
    @IBOutlet weak var summeryTitle: UILabel!
    @IBOutlet weak var summeryTextView: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetUp()
    }

     func initialSetUp() {
        titleLabel?.text = nil
        desLabel?.text = nil
        movieImageView?.image = nil
        releaseDateLable.text = nil
        summeryTextView.text = nil
        summeryTitle.text = nil
    }
     //fileprivate
     func setUpImage(_ data: Entry) {
        if let imageString = data.imImage?.last?.label {
            if let url = URL(string: imageString) {
//                self.movieImageView.kf.indicatorType = .activity
                self.movieImageView.kf.setImage(with: url)
                Mint.sharedInstance()?.logEvent(withName: "Setup image")
            }
        }
    }

    func populateMoviesDetailsInfo(_ entry: Entry?) {
        if let data = entry {
            titleLabel?.text = "Title:  \(data.title?.label ?? "")"
            desLabel?.text = "Category:  \(data.category?.attributes.label ?? "")"
            releaseDateLable.text = "Release Date:  \(data.imReleaseDate?.attributes.label ?? "")"
            summeryTextView.text = data.summary?.label
            summeryTitle.text = "Summary:"
            setUpImage(data)
            Mint.sharedInstance()?.logEvent(withName: "Populated data")
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
