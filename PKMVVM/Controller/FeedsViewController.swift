//
//  FeedsViewController.swift
//  PKMVVM
//
//  Created by Divya Kothagattu on 02/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit
import SplunkMint

class FeedsViewController: UIViewController {

    @IBOutlet var feedsView: FeedsView!
    var feedsViewModel = FeedsViewModel()
    let alertView: PKAlertViewControllerProtocol.Type = UIAlertController.self
    override func viewDidLoad() {
        super.viewDidLoad()
        if !Rechability.isConnectedToNetwork() {
            alertView.submitAlertView(viewController: self, title: "No Internet Connection..!", message: "Make sure your device is connected to the internet.") { _ in
            }
        }
        feedsViewModel.dataSource = feedsView
        HudProgressView.shared.showHudProgressView()
        feedsViewModel.getUpdatedMovies()
        feedsView?.selectedFeedDelegate = self
        feedsView?.searchTappedDelegate = self
        feedsViewModel.alertDelegate = self
        Mint.sharedInstance()?.logEvent(withName: "feedsView")
    }
    deinit {
        print("memory issues in \(FeedsViewController.self)")
    }
}

extension FeedsViewController: SelectedFeedDelegate {
    func didSelectRow(_ entry: Entry?) {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        if  let controller = storyBoard.instantiateViewController(withIdentifier: "FeedDetailViewController") as? FeedDetailViewController {
            controller.populateDetail(entry)
            navigationController?.pushViewController(controller, animated: true)
            Mint.sharedInstance()?.logEvent(withName: "Did tapped on tableview cell")
        }
    }
}

extension FeedsViewController: MovieAlertProtocol {
    func showFailureAlert(completionHandler: @escaping (Bool) -> Void) {
               alertView.submitAlertView(viewController: self, title: "No Internet Connection..!", message: "Make sure your device is connected to the internet.") { _ in
        }
      }
    }
extension FeedsViewController: SearchTappedDelegate {
    func didTappedOnSsearch(searchActive: Bool) {

    }
}
