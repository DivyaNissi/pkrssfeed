//
//  FeedDetailViewController.swift
//  PKMVVM
//
//  Created by Divya Kothagattu on 04/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit
import SplunkMint

class FeedDetailViewController: UIViewController {
    @IBOutlet  var detailsView: FeedDetailView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Detailview"
        Mint.sharedInstance()?.logEvent(withName: "Loaded ViewDidLoad")
        changeBarbuttonImage()
    }
    func changeBarbuttonImage() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "leftArrow"), style: .plain, target: self, action: #selector(backButtonTapped))
        navigationItem.leftBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    @objc func backButtonTapped() {
        navigationController?.popViewController(animated: true)
    }
     func populateDetail(_ entry: Entry?) {
        DispatchQueue.main.async {
            self.detailsView?.popuLateData(entry)
            Mint.sharedInstance()?.logEvent(withName: "Populated selected details")
        }
    }
    deinit {
        detailsView = nil
        detailsView?.dataSource = nil
        detailsView?.feedsDetailsTableView = nil
        print("memory issues in \(FeedDetailViewController.self)")
    }

}
