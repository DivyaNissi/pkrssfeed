//
//  APIConfig.swift
//  PKMVVM
//
//  Created by Divya Kothagattu on 03/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import Foundation

struct EndPoint {
    static let basicUrl = "https://itunes.apple.com/us/rss/topmovies/limit=30/json"
}
