//
//  ServiceHandler.swift
//  PKMVVM
//
//  Created by Divya Kothagattu on 03/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import Foundation

class ServiceHandler {
    typealias CompletionBlock = ([Entry], Bool) -> Void
    var movieNamesArray = [String]()

    func getDataFromApi (withUrl baseUrl : String, completionBlock : @escaping CompletionBlock) {
        if let mainUrl = URL(string: baseUrl) {
            URLSession.shared.dataTask(with: mainUrl, completionHandler: { (data, _, error) in
                if let data = data {
                    do {
                        let feeds = try JSONDecoder().decode(FeedsModel.self, from: data)
                        completionBlock(feeds.feed.entry, true)
                    } catch {
                        completionBlock([], false)
                    }
                } else {
                    completionBlock([], false)
                }
            }).resume()
        }
    }
}
