//
//  FeedsModel.swift
//  PKMVVM
//
//  Created by Divya Kothagattu on 02/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import Foundation

// MARK: - Users
public struct FeedsModel: Codable {
    let feed: Feed
}

// MARK: - Feed
public struct Feed: Codable {
    let entry: [Entry]
}

// MARK: - Entry
public struct Entry: Codable {
    let imImage: [IMImage]?
    let summary: IMArtist?
    let title: IMArtist?
    let id: ID?
    let category: Category?
    let imReleaseDate: IMReleaseDate?

    public enum CodingKeys: String, CodingKey {
        case imImage = "im:image"
        case summary
        case title, id
        case category
        case imReleaseDate = "im:releaseDate"
    }
}

// MARK: - Category
public struct Category: Codable {
    let attributes: CategoryAttributes
}

// MARK: - CategoryAttributes
public struct CategoryAttributes: Codable {
    let imID, term: String
    let scheme: String
    let label: String

  public  enum CodingKeys: String, CodingKey {
        case imID = "im:id"
        case term, scheme, label
    }
}

// MARK: - ID
public struct ID: Codable {
    let label: String
    let attributes: IDAttributes
}

// MARK: - IDAttributes
public struct IDAttributes: Codable {
    let imID: String

   public enum CodingKeys: String, CodingKey {
        case imID = "im:id"
    }
}

// MARK: - IMArtist
public struct IMArtist: Codable {
    let label: String
}

// MARK: - IMContentType
public struct IMContentType: Codable {
    let attributes: IMContentTypeAttributes
}

// MARK: - IMContentTypeAttributes
public struct IMContentTypeAttributes: Codable {
    let term, label: String
}

// MARK: - IMImage
public struct IMImage: Codable {
    let label: String
    let attributes: IMImageAttributes
}

// MARK: - IMImageAttributes
public struct IMImageAttributes: Codable {
    let height: String
}

// MARK: - IMPrice
public struct IMPrice: Codable {
    let label: String
    let attributes: IMPriceAttributes
}

// MARK: - IMPriceAttributes
public struct IMPriceAttributes: Codable {
    let amount, currency: String
}

// MARK: - IMReleaseDate
public struct IMReleaseDate: Codable {
    let label: String
    let attributes: IMArtist
}

// MARK: - Link
public struct Link: Codable {
    let attributes: LinkAttributes
    let imDuration: IMArtist?

    enum CodingKeys: String, CodingKey {
        case attributes
        case imDuration = "im:duration"
    }
}

// MARK: - LinkAttributes
public struct LinkAttributes: Codable {
    let rel, type: String
    let href: String
    let title, imAssetType: String?

   public  enum CodingKeys: String, CodingKey {
        case rel, type, href, title
        case imAssetType = "im:assetType"
    }
}
