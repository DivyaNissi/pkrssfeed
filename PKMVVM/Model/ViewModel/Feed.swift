//
//  Feed.swift
//  PK-MVVM
//
//  Created by Divya Kothagattu on 03/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import Foundation

struct Feed: Codable {
    let feed: Feeds
}

struct Feeds: Codable {
    let author: Author
    let entry: [Entry]?
    let title: Title?
}

struct Author: Codable {

}

struct Entry: Codable {
    let imImage: [ImImage]?
    let title: Title?
    let category: Category?
    let releaseDate: ReleaseDate?

    enum CodingKeys: String, CodingKey {
        case releaseDate = "im:releaseDate"
        case imImage = "im:image"
        case title
        case category
    }
}

struct ImImage: Codable {
    let label: String
}

struct Summary: Codable {

}

struct Category: Codable {
    let attributes: Attributes?
}

struct Attributes: Codable {
    let term: String?
    let label: String?
}

struct Title: Codable {
    let label: String?
}

struct ReleaseDate: Codable {
    let label: String
    let attributes: Attributes
}
