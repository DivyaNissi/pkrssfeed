//
//  FeedsViewModel.swift
//  PKMVVM
//
//  Created by Divya Kothagattu on 02/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import Foundation

protocol MovieAlertProtocol: class {
    func showFailureAlert(completionHandler: @escaping(Bool) -> Void)
}

protocol FetchDataProtocol: class {
    func getUpdatedMovies()
}
//protocol UpdatedDataProtocol: class {
//    func didUpdateMovies(movies: [Entry])
//}

protocol DataSource: class {
    func handleMovies(movies: [Entry])
}
class FeedsViewModel: NSObject, FetchDataProtocol {
    weak var alertDelegate: MovieAlertProtocol?
    weak var dataSource: DataSource?
    var view: FeedsView?
  //  var consumer: UpdatedDataProtocol?
    var apiHandler = ServiceHandler()
    var moviesArray = [Entry]()

    func getUpdatedMovies() {
        HudProgressView.shared.showHudProgressView()
        guard Rechability.isConnectedToNetwork() else {
            HudProgressView.shared.hideHudProgressView()
            self.alertDelegate?.showFailureAlert(completionHandler: { _ in })
            return
        }
        getDataFromAPIHandler(url: EndPoint.basicUrl)
    }

      private func getDataFromAPIHandler(url : String) {
        apiHandler.getDataFromApi(withUrl: url) { [weak self] (moviesArray, status) in
            guard let self = self else {return}
            HudProgressView.shared.hideHudProgressView()
            if status == true {
                self.moviesArray = moviesArray
                self.dataSource?.handleMovies(movies: self.moviesArray)
            }  else  {
                HudProgressView.shared.hideHudProgressView()
                self.alertDelegate?.showFailureAlert(completionHandler: { _ in })
                return
            }
         }
    }
}
